#ifndef GAME_SERVER_ENTITIES_WALL_H
#define GAME_SERVER_ENTITIES_WALL_H

#include <game/server/entity.h>

class CWall : public CEntity
{
public:
	CWall(CGameWorld *pGameWorld, vec2 Pos, vec2 Direction, float StartEnergy, int Owner);
	
	virtual void Reset();
	virtual void Tick();
	virtual void Snap(int SnappingClient);
	vec2 m_StartPos;
	vec2 m_Pos;
	
	vec2 m_Dir;	
	inline int GetOwner(){return m_Owner;};
protected:
	bool HitCharacter(vec2 From, vec2 To);
	void DoBounce();
	
private:	
	
	float m_Energy;
	int m_Bounces;
	int m_EvalTick;
	int m_Owner;
	int m_NumParts;	
	enum
	{
	STATE_NONE = 0,
	STATE_STUCK,
	STATE_NORMAL,
	NUM_STATES,
	
	NUM_STATS = 16,		
	MAX_PARTS = 10,	
	};
	
	struct PlayerStat
	{
		int m_State;
		int m_StartTick;
		int m_Duration;
		int m_Part;
		int m_Side;		
		vec2 m_Pos;
		vec2 m_Point;
		float m_Angle;
		float m_Dist;
		int m_LastTick;
		int m_LastSide;
		vec2 m_StuckVel;
	
		CCharacter *pChar;
		
	} m_aStats[NUM_STATS];
	
};

#endif
