// copyright (c) 2007 magnus auvinen, see licence.txt for more info
#include <game/generated/protocol.h>
#include <game/server/gamecontext.h>
#include "wall.h"

CWall::CWall(CGameWorld *pGameWorld, vec2 Pos, vec2 Direction, float StartEnergy, int Owner)
: CEntity(pGameWorld, NETOBJTYPE_LASER)
{
	m_StartPos = Pos;
	m_Owner = Owner;
	m_Energy = StartEnergy;
	m_Dir = Direction;
	m_Bounces = 0;
	m_EvalTick = 0;
	m_Pos = m_StartPos + m_Dir * m_Energy;
	GameWorld()->InsertEntity(this);
	m_NumParts = 1;
	//DoBounce();
}
void CWall::Reset()
{
	GameServer()->m_World.DestroyEntity(this);
}

void CWall::Tick(){
	
	CCharacter *OwnerChar = GameServer()->GetPlayerChar(m_Owner);
	if(!OwnerChar || !OwnerChar->IsAlive())
	Reset();
}

void CWall::Snap(int SnappingClient)
{
	
	//if(NetworkClipped(SnappingClient))
	//	return;
	CNetObj_Laser *pObj = static_cast<CNetObj_Laser *>(Server()->SnapNewItem(NETOBJTYPE_LASER, m_ID, sizeof(CNetObj_Laser)));
	pObj->m_X = (int)m_Pos.x;
	pObj->m_Y = (int)m_Pos.y;
	pObj->m_FromX = (int)m_StartPos.x;
	pObj->m_FromY = (int)m_StartPos.y;
	pObj->m_StartTick = Server()->Tick()-1;
}
